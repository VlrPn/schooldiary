﻿using SchoolDiary.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolDiary.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DayView : ContentView
    {

        public DayView(DayViewModel vm)
        {
            InitializeComponent();

            BindingContext = vm;

            for(int i = 0; i < vm.Titles.Count; i++)
            {
                Button button = new Button { Text = vm.Titles[i], BackgroundColor = Color.White };

                titlesGrid.Children.Add(button, 0, i);
            }
            
        }

    }
}