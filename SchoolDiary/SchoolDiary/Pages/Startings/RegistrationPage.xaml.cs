﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolDiary.Pages.Startings
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        public RegistrationPage()
        {
            InitializeComponent();
        }

        private async void EnterButton_Clicked(object sender, EventArgs e)
        {
            App.Current.Properties["StartPage"] = "Main";

            await Shell.Current.GoToAsync("//Main");
        }
    }
}