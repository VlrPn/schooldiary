﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolDiary.Pages.Startings
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private string login = "";
        private string password = "";


        public LoginPage()
        {
            InitializeComponent();
        }

        private async void EnterButton_Clicked(object sender, EventArgs e)
        {
            if (login == "" && password == "")
            {
                App.Current.Properties["StartPage"] = "Main";

                await Shell.Current.GoToAsync("//Main");
            }
        }

        private void LabelForgetTapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

        }

    }
}