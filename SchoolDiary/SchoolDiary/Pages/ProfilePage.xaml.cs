﻿using SchoolDiary.Pages.Startings;
using SchoolDiary.ViewModels;
using SchoolDiary.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolDiary.Pages
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }

        private async void SettingsButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new SettingsPage());
        }

        private async void ExitButton_Clicked(object sender, EventArgs e)
        {
            App.Current.Properties["StartPage"] = "Login";

            await Shell.Current.GoToAsync("//Login");
        }
    }


}

