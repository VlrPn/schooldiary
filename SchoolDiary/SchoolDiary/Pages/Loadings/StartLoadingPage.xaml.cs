﻿using SchoolDiary.Pages;
using SchoolDiary.Pages.Startings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolDiary.Pages.Loadings
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartLoadingPage : ContentPage
    {
        public StartLoadingPage()
        {
            InitializeComponent();

            Loading();
        }

        private async void Loading()
        {
            await Task.Delay(2000);

            if (App.Current.Properties.TryGetValue("StartPage", out object name))
            {
                // выполняем действия, если в словаре есть ключ "RootPage"
                await Shell.Current.GoToAsync($"//{name.ToString()}");
            }
            else
            {
                await Shell.Current.GoToAsync("//Login");
            }
        }
    }
}