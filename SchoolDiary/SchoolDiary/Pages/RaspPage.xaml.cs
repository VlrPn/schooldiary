﻿using SchoolDiary.ViewModels;
using SchoolDiary.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolDiary.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RaspPage : ContentPage
    {
        private DateTime firstDay, secondDay;
        private int cd = 7;

        private bool active = false;

        public RaspPage()
        {
            InitializeComponent();

            //Заплатка выбора дней
            SelectDays(null, null);
        }

        private void SelectDays(object sender, EventArgs e)
        {
            if (!active)
            {
                active = true;

                firstDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                secondDay = firstDay.AddDays(7);

                DayCounter.Text = $"С {firstDay:d} по {secondDay:d}";

                cd = secondDay.Subtract(firstDay).Days;

                GetDays();
            }
        }

        private async void GetDays()
        {
            freeStack.Children.Clear();

            for (int i = 0; i < cd; i++)
            {
                DayViewModel dayViewModel = await Task.Run(() => CreateDay(firstDay.AddDays(i)));

                freeStack.Children.Add(new DayView(dayViewModel));
            }

            active = false;
        }

        private DayViewModel CreateDay(DateTime date)
        {
            List<string> st = new List<string>();

            var rand = new Random();
            int n = rand.Next(1, 9);

            for (int x = 0; x < n; x++)
            {
                st.Add($"Название предмета {x + 1}");
            }

            return new DayViewModel { Year = date.Year, Month = date.Month, Number = date.Day, Titles = st };
        }
    }
}