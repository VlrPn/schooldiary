﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolDiary.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        OSAppTheme currentTheme;

        public SettingsPage()
        {
            InitializeComponent();

            currentTheme = Application.Current.RequestedTheme;
        }

        private void Button_Reset(object sender, EventArgs e)
        {
            App.Current.Properties.Clear();
        }

        public async void Dismiss(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

    }
}