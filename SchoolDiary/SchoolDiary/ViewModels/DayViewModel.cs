﻿using SchoolDiary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SchoolDiary.ViewModels
{
    public class DayViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private Day Day { get; set; }

        public DayViewModel()
        {
            Day = new Day();
        }

        public int Number
        {
            get { return Day.Number; }
            set
            {
                if(Day.Number != value)
                {
                    Day.Number = value;
                    OnPropertyChanged("DayNum");

                    NumberName = Day.NameDay(value, Month, Year);
                }
            }
        }

        public string NumberName
        {
            get { return Day.NumberName; }
            set
            {
                if (Day.NumberName != value)
                {
                    Day.NumberName = value;
                    OnPropertyChanged("DayName");
                }
            }
        }

        public int Month
        {
            get { return Day.Month; }
            set
            {
                if (Day.Month != value)
                {
                    Day.Month = value;
                    OnPropertyChanged("MonthNum");

                    MonthName = Day.NameMonth(value);
                }
            }
        }

        public string MonthName
        {
            get { return Day.MonthName; }
            set
            {
                if (Day.MonthName != value)
                {
                    Day.MonthName = value;
                    OnPropertyChanged("MonthName");
                }
            }
        }

        public int Year
        {
            get { return Day.Year; }
            set
            {
                if (Day.Year != value)
                {
                    Day.Year = value;
                    OnPropertyChanged("Year");
                }
            }
        }

        public List<string> Titles
        {
            get { return Day.Titles; }
            set
            {
                if (Day.Titles != value)
                {
                    Day.Titles = value;
                    OnPropertyChanged("Titles");
                }
            }
        }



        public void OnPropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

    }
}
