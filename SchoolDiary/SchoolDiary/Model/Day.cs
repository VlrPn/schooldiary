﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolDiary.Model
{
    class Day
    {
        public int Number { get; set; } = 1;

        public string NumberName { get; set; } = NameDay(1, 1, 2000);

        public int Month { get; set; } = 1;

        public string MonthName { get; set; } = NameMonth(1);

        public int Year { get; set; } = 2000;

        public List<string> Titles { get; set; } = new List<string>();



        public static string NameMonth(int month)
        {
            switch (month)
            {
                case 1:
                    {
                        return "Января";
                    }
                case 2:
                    {
                        return "Февраля";
                    }
                case 3:
                    {
                        return "Марта";
                    }
                case 4:
                    {
                        return "Апреля";
                    }
                case 5:
                    {
                        return "Мая";
                    }
                case 6:
                    {
                        return "Июня";
                    }
                case 7:
                    {
                        return "Июля";
                    }
                case 8:
                    {
                        return "Августа";
                    }
                case 9:
                    {
                        return "Сентября";
                    }
                case 10:
                    {
                        return "Октября";
                    }
                case 11:
                    {
                        return "Ноября";
                    }
                case 12:
                    {
                        return "Декабря";
                    }
                default:
                    {
                        return "Error404";
                    }
            }
        }

        public static string NameDay(int day, int month, int year)
        {
            DateTime date = new DateTime(year, month, day);

            switch ((int)date.DayOfWeek)
            {
                case 0:
                    {
                        return "Вс.";
                    }
                case 1:
                    {
                        return "Пн.";
                    }
                case 2:
                    {
                        return "Вт.";
                    }
                case 3:
                    {
                        return "Ср.";
                    }
                case 4:
                    {
                        return "Чт.";
                    }
                case 5:
                    {
                        return "Пт.";
                    }
                case 6:
                    {
                        return "Сб.";
                    }
                default:
                    {
                        return "err";
                    }
            }
        }
    }
}
